$(function () {
    var contextElement = document.getElementById("context-menu");    

    display(false);
    //Called from "function SetDisplay(bool)" in lua .. sends the NUI status.
    //Called from "function SendObjectData()" in lua .. Sent the corrisponding data in this JS. 
    window.addEventListener('message', function(event) {
        var item = event.data;
		
		if (item.type == "UpdateUI")
		{
			if (item.show == true) {
				console.log("Showing display")
				display(true);				
			} else if (item.show == false) {
				display(false);
				clearContextmenu();
			}
		}
		if (item.type == "UpdateMenuItems")
		{
			updateMenuItems(item.menuItems);
			console.log("Updating menu items");
			createMenu();
		}
    });

    // if the person uses the escape or Z key, it will exit the resource
    //Does a Callback to the lua to CLOSE the NUI.
    //27 = ESC , 90 = Z
    document.onkeyup = function (data) {
        if (data.which == 27 || data.which == 90) 
        {                      
            clearContextmenu();
            $.post(`https://${GetParentResourceName()}/closeNUI`, JSON.stringify({}));
            return
        }
    };
    //Callbacks to contextmenu-c.lua
    //e.target.id is the <div> id = "XXXXXXXX" </div> on the element
    //-----------------------------------------------------------------
    // $.post('https://contextmenu/use', JSON.stringify({}));
    // return
    //- Calls back the specific function in the lua script -> RegisterNUICallback("use", function(data) ............. end) etc.
    //-----------------------------------------------------------------
    //Add NEW cases for any NEW menu items that are created so they can do Callbacks to the LUA.
    document.addEventListener("click", (e) => {
		if (e.target != null) {
			var idMap = e.target.id.split(".");
			var currentItem = menuItems[idMap[0]];
			for (var i = 1; i < idMap.length; i++) {
				currentItem = currentItem.subMenuItems[idMap[i]];
			}
			if (currentItem != null) {
				$.post(`https://${GetParentResourceName()}/${currentItem.callback}`, JSON.stringify({}))
			}
		}       
    });	
   
    //Context menu -- right click
    window.addEventListener("contextmenu",function(event){
		console.log("right clicking");
        event.preventDefault();
        $.post(`https://${GetParentResourceName()}/rightClick`, JSON.stringify({}));
        contextElement.style.top = event.offsetY + "px";
        contextElement.style.left = event.offsetX + "px";
    });
    window.addEventListener("click",function(){
        document.getElementById("context-menu").classList.remove("active");
    });
	
	$.post(`https://${GetParentResourceName()}/notifyNUIReady`, JSON.stringify({}))
});

var menuItems = [];
//expected menuItem format  { text, callback, illegal, subMenuItems[] }

//Makes the first letter in a string Uppercase.
function upperCaseFirstLetter(string) 
{
	return string.charAt(0).toUpperCase() + string.slice(1);
}

function updateMenuItems(menuItems) 
{
	this.menuItems = menuItems;
}

function createMenu()
{
	clearContextmenu();
	document.getElementById("context-menu").classList.add("active");
	for (var i = 0; i < menuItems.length; i++) {
		createMenuItem(i.toString(), menuItems[i], "context-menu");
	}	
}

//Clears the menu on exit or new generated menu so the elements dont persist through new menus (stacking menus infinitely)
function clearContextmenu()
{
	var item = document.getElementById("context-menu");
	item.innerHTML = ''; //<--- Look into a CLEANER option.
	item.classList.remove("active");
}

function createMenuItem(id, menuItem, parentNode)
{	
	console.log("Adding menu item : " + id + " : " + menuItem.text);
	node = document.createElement("div");
	textnode = document.createTextNode(menuItem.text);
	var classes = "menu-item";
	if (id.includes(".")) {
		classes += " sub-menu-item";
	}
	if (menuItem.subMenuItems && menuItem.subMenuItems.length) {
		classes += " sub-menu-parent";
	}
	if (menuItem.illegal) {
		classes += " illegal";
	}
	node.setAttribute("id", id);
	node.setAttribute("class", classes);
	node.appendChild(textnode);
	document.getElementById(parentNode).appendChild(node);
	if (menuItem.subMenuItems) {
		for (var i = 0; i < menuItem.subMenuItems.length; i++)
		{
			createMenuItem(id.toString() + "." + i.toString(), menuItem.subMenuItems[i], id);
		}
	}
}

function display(bool) {
	if (bool) {
		$("#container").show();
		$("#close").show();
	} else {
		$("#container").hide();
		$("#close").hide();
	}
}