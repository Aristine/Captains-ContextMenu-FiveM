local NuiIsReady = false
local display = false


function SetDisplay(state)
    display = state
    SetNuiFocus(state, state)
    SendNUIMessage({
        type = "UpdateUI",
        show = state,
    })
end

function SetMenuItems(items)
	SendNUIMessage({
		type = "UpdateMenuItems",
		menuItems = items
	})
end

RegisterNUICallback("notifyNUIReady", function(data)
	NuiIsReady = true
end)

RegisterNUICallback("rightClick", function(data)
	startRaycast = true
end)

RegisterNUICallback("closeNUI", function(data)
	SetDisplay(false)
	startRaycast = false
end)

Citizen.CreateThread(function()
	while not NuiIsReady do Citizen.Wait(1) end
	while true do Citizen.Wait(1)
	
		if IsControlJustReleased(1, z_key) then            
            SetDisplay(not display)
        end
	
		if display then
			DisableControlAction(0, 1, true) -- LookLeftRight
            DisableControlAction(0, 2, true) -- LookUpDown
            DisableControlAction(0, 142, true) -- MeleeAttackAlternate
            DisableControlAction(0, 18, true) -- Enter
            DisableControlAction(0, 322, true) -- ESC
            DisableControlAction(0, 106, true) -- VehicleMouseControlOverride
		end
	end
end)

